drop database if exists covid19;

create database covid19;

use covid19;

create table tcovid_cases (
id INT PRIMARY KEY AUTO_INCREMENT,
Case_Type varchar(256),
People_Total_Tested_Count int,
Cases int,
Difference int,
Date Date,
Combined_Key varchar(100),
Country_Region varchar(100),
Province_State varchar(100),
Admin2 varchar(100),
iso2 varchar(100),
iso3 varchar(100),
FIPS varchar(100),
Latitude float,
Longitude float,
Population_Count int,
People_Hospitalized_Cumulative_Count int,
Data_Source varchar(512),
Prep_Flow_Runtime varchar(512)
);

CREATE USER 'dataloader'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON covid19.* TO 'dataloader'@'localhost';
FLUSH PRIVILEGES;