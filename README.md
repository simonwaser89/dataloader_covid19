# covid_dataloader

This project is part of the Software Engineering course
in the Master Automation Management program at the FHNW.

## Java version
Java 15.0.1 is used in the project.

## Build instruction
ANT: ant clean package

## Dependencies
There are 4 jar files which are needed:

* commons-io-2.8.0.jar
* commons-lang3-3.11.jar
* mysql-connector-java-8.0.22.jar
* opencsv-5.3.jar

## Running
java -jar DataLoader_PROD_1_0.jar app.properties

The app.properties must contain the following information:

* dataFile (contains the data file to load)
* hostname (host where the database is running)
* port (port on which the database is running)
* dbname (name of the database)
* username (username to access the database)
* password (password to access the database)

